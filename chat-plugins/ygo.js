'use strict';

const https = require('https');

// error handling what?
let getCard = (card) => {
	return new Promise((resolve, reject) => {
		https.get(`https://www.ygohub.com/api/card_info?name=${card}`, (res) => {
			res.setEncoding('utf8');

			let data = "";
			res.on('data', (chunk) => {
				data += chunk;
			});

			res.on('end', () => {
				resolve(JSON.parse(data));
			});
		});
	});
};

exports.commands = {
	ygo: function(target, room, user) {
		this.can("broadcast");

		if (!target) return this.send("``ygo [card]``");
		if (target.length > 50) return this.send("``50 characters is the limit!``");

		let card = encodeURI(target);

		getCard(card)
			.then((data) => {
				if (data.status === "error") return this.send("invalid card id");

				let cardData = data.card;

				for (let i = 0, len = cardData.stars; i < len; i++) {
					if (i === 0) cardData.stars += " => ";

					cardData.stars += "<img src=\"https://vignette.wikia.nocookie.net/yugioh/images/e/e3/CG_Star.svg/revision/latest/scale-to-width-down/18?cb=20120918052836\" width=\"12\" height=\"12\" />";
					
					if ((i - 1) !== len) cardData.stars += "&nbsp;";
				}

				let display = `<div style="width: 100%; padding: 2px; background: #DDD; border: 1px solid #999; font-family: courier;"><div style="width: 50%; float: left;"><strong>Name: </strong>${cardData.name}<br /><strong>Type: </strong>${cardData.type}<br /><strong>Level: </strong>${cardData.stars ? cardData.stars : "-"}<br /><br /><div style="padding: 2px; background: #FFF; border: 1px solid #AAA;"><strong>[${cardData.species ? cardData.species : " - "} / ${cardData.monster_types ? ((cardData.monster_types).join(" / ")) : " - "}]</strong><br /><em>${(cardData.has_materials ? "(" + cardData.materials + ")<br />" : "")}</em>${cardData.text}</div></div><img style="float: right;" src="${cardData.thumbnail_path}" width="136" height="200" /><div style="clear: both;"></div></div><br />`;
				
				return this.send(`/addhtmlbox ${display}`);
			});
	}
};